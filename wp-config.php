<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'projetos_esdrasobras_site' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '_jVQ}7Yem.f4>&b5A#r-qK??C&QL&tL8mjL<&yUD=w7>%h$<021I0 `FIK1@p[}2' );
define( 'SECURE_AUTH_KEY',  'Be4*BMAW9xK+h8Gj:/@e{[u8}k5-@Ceq4/Sc)&9Il}$~Bp1T#$4{:$@CPxu@B!.*' );
define( 'LOGGED_IN_KEY',    'l9%;yb$[rAj(R_oy#<PXZD449-c1{VVXrDmT=7eLSVUzNYFEijWiP;EgzSer x)y' );
define( 'NONCE_KEY',        'a/xZr)15dmua5~nJlR3@?m$m7k`h!rd@r!#|j5AhzplOcb8O}u*C]!GQ@#A4u%?W' );
define( 'AUTH_SALT',        'Q rN}U9vDLjCsg589q,iL(Ifn..=JY6kYcTH0eK{E[S-xX<m%.^Fc ^gUZ5w,[;^' );
define( 'SECURE_AUTH_SALT', 'w/8@RPM~yKou5VF]@@3bWrsLK$4#OPm5dIn>v^,,y(csB!xwpv|_ep]Z3qE%X/?D' );
define( 'LOGGED_IN_SALT',   'ico~ci|-=]t]GS:gUp<8Rs]e9o*KAqYCjVny9w&cb:3p#H4c{Tj9E+vdX?k]u+lI' );
define( 'NONCE_SALT',       'LJ  d@`gq%h^11u45}wNRB9Uz.2?]tw`ec&/(n|id*LV1$E|yf*ab7]]J,Rwm6<4' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'eo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
