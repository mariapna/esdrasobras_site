<?php

/**
 * Template Name: Inicial
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package esdrasobras
 */

get_header();
;
?>

<!-- PÁGINA INICIAL -->
<div class="pg pg-inicial">
	<div class="containerFull">
		<div class="inicial">

			<div class="texto-topo">
				<h1>absorvemos<br> todo o ônus da<br> gestão de mão<br> de obra</h1>
				<p class="texto">Elimine custos com demandas trabalhistas, otimize tempo na gestão e<br> fique tranquilo com a segurança dos trabalhadores, todos estão totalmente<br> segurados contra invalidez e acidentes. Absorvemos todo o ônus da gestão da<br> mão da obra dando a você todo o tempo necessário para execução da obra.</p>
				<div class="button"><a href="#">Saiba mais</a></div>
			</div>

			<ul class="projetos">
				<li class="projeto">
					<a href="#">
						<figure>
							<img src="<?= get_template_directory_uri() . '/img/projeto-1.png'; ?>" alt="Projeto">
							<figcaption class="hidden">Projeto</figcaption>
						</figure>
					</a>
					<h3 class="titulo-secundario">residencial milano</h3>
					<p class="texto">Apartamentos com 44m de área privativa, com vaga de garagem marcada. 2 Salões de festas com quadra poliesportiva e portaria 24 horas.</p>
					<a href="#" class="saiba-mais">saiba mais</a>
				</li>
				<li class="projeto">
					<a href="#">
						<figure>
							<img src="<?= get_template_directory_uri() . '/img/projeto-2.jpg'; ?>" alt="Projeto">
							<figcaption class="hidden">Projeto</figcaption>
						</figure>
					</a>
					<h3 class="titulo-secundario">residencial safira</h3>
					<p class="texto">Apartamentos com 44,50m2 de área privativa, em condomínio vertical fechado, com vaga de garagem. Quadra poliesportiva salão de festas e portaria 24 horas.</p>
					<a href="#" class="saiba-mais">saiba mais</a>
				</li>
				<li class="projeto">
					<a href="#">
						<figure>
							<img src="<?= get_template_directory_uri() . '/img/projeto-3.png'; ?>" alt="Projeto">
							<figcaption class="hidden">Projeto</figcaption>
						</figure>
					</a>
					<h3 class="titulo-secundario">construímos sua casa</h3>
					<p class="texto">Quer construir sua casa, mas não quer se preocupar com a execução do projeto, fala com a Esdras. Projetos particulares com prazo, garantia e qualidade.</p>
					<a href="#" class="saiba-mais">saiba mais</a>
				</li>
			</ul>

			<a href="#" class="sobre-a-esdras">
				<figure>
					<img src="<?= get_template_directory_uri() . '/img/sobre-a-esdras.png'; ?>" alt="">
				</figure>
			</a>

			<section class="secao-vantagens">
				<h6 class="hidden">SEÇÃO DE VANTAGENS</h6>
				<h2>vantagens esdras</h2>
				<span>conheça alguns benefícios</span>

				<ul class="vantagens">
					<li class="vantagem">
						<img src="<?= get_template_directory_uri() . '/img/icone-1.png'; ?>" alt="Isenção trabalhista">
						<h3 class="titulo-secundario">isenção trabalhista</h3>
						<p class="texto">Elimine os custos de rescisões, demandas trabalhistas, pagamentos de encargos e suporte ao colaborador.</p>
					</li>
					<li class="vantagem">
						<img src="<?= get_template_directory_uri() . '/img/icone-2.png'; ?>" alt="Trabalhador segurado">
						<h3 class="titulo-secundario">Trabalhador segurado</h3>
						<p class="texto">Todos os colaboradores estão segurados por nós em contrato. Oferecemos suporte total em qualquer ocorrência.</p>
					</li>
					<li class="vantagem">
						<img src="<?= get_template_directory_uri() . '/img/icone-3.png'; ?>" alt="Pagamento por diária">
						<h3 class="titulo-secundario">Pagamento por diária</h3>
						<p class="texto">Negociação de contratos flexíveis, pague apenas pelo período trabalhado. Negocie por diárias.</p>
					</li>
					<li class="vantagem">
						<img src="<?= get_template_directory_uri() . '/img/icone-4.png'; ?>" alt="Mão de sob-demanda">
						<h3 class="titulo-secundario">Mão de sob-demanda</h3>
						<p class="texto">Amplie ou reduza seu quadro de funcionários conforme necessidade, tudo em contratos flexíveis e pagamento por medição.</p>
					</li>
					<li class="vantagem">
						<img src="<?= get_template_directory_uri() . '/img/icone-5.png'; ?>" alt="Uniforme personalizado">
						<h3 class="titulo-secundario">Uniforme personalizado</h3>
						<p class="texto">Padronização de traje de funcionários. Uniforme personalizado.</p>
					</li>
					<li class="vantagem">
						<img src="<?= get_template_directory_uri() . '/img/icone-6.png'; ?>" alt="E.P.I. básico">
						<h3 class="titulo-secundario">E.P.I. básico</h3>
						<p class="texto">Equipamentos básicos de proteção fornecidos por nós.</p>
					</li>
				</ul>
			</section>

			<figure class="clientes">
				<img src="<?= get_template_directory_uri() . '/img/clientes.png'; ?>" alt="Clientes">
			</figure>

		</div>
	</div>
</div>

<?php get_footer();