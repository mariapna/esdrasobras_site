<?php

/**
 * Template Name: Serviços
 * Description: Página Serviços
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package esdrasobras
 */

get_header();

?>

<!-- PÁGINA DE SERVIÇOS -->
<div class="pg pg-servicos">
	<div class="containerFull">
		<div class="servicos">
			<div class="texto-topo">
				<h1>serviços que a equipe oferece.</h1>
				<p class="texto">Nossos profissionais são devidamente treinados e estão qualificados para
				atuar em todas as áreas da contrução cívil.</p>
			</div>
			<figure class="esdras-obras">
				<img src="<?= get_template_directory_uri() . '/img/img-pg-servicos.png'; ?>" alt="Imagem serviços">
			</figure>
			<section class="secao-vantagens">
				<h6 class="hidden">SEÇÃO DE VANTAGENS</h6>
				<div class="titulo">
					<h2 class="titulo-secundario">vantagens</h2>
				</div>
				<article class="desc-vantagens">
					<div class="paragrafo">
						<p class="texto">Elimine custos com demandas trabalhistas, otimize tempo na gestão e fique tranquilo com a segurança dos trabalhadores, todos estão totalmente segurados contra invalidez e acidentes.</p>
					</div>
					<div class="paragrafo">
						<p class="texto"> Absorvemos todo o ônus da gestão da mão da obra dando a você todo o tempo necessário para execução da obra.</p>
					</div>
				</article>
				<div class="topicos">
					<ul>
						<li>
							Seguro por invalidez ou por fatalidade
							<p class="paragrafo texto">Todos os colaboradores estão segurados por nós em contrato. Oferecemos suporte total em qualquer ocorrência.</p>
						</li>
						<li>
							Mão de Obra Sob-Demanda
							<p class="paragrafo texto"> Amplie ou reduza seu quadro de funcionários conforme necessidade, tudo em contratos flexíveis e pagamento por medição.</p>
						</li>
						<li>
							Isenção Trabalhista
							<p class="paragrafo texto">Elimine os custos de rescisões, demandas trabalhistas, pagamentos de encargos e suporte ao colaborador.</p>
						</li>
						<li>
							Uniforme e EPI Básico
							<p class="paragrafo texto">Padronização de traje de funcionários. Uniforme personalizado e equipamentos básicos de proteção fornecidos por nós.</p>
						</li>
					</ul>
				</div>
			</section>

			<figure class="img-servicos">
				<img src="<?= get_template_directory_uri() . '/img/img-servicos.png'; ?>" alt="Serviços">
			</figure>

			<div class="video-como">
				<h4 class="titulo-secundario">como trabalhamos</h4>
				<div class="video">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/ScwNcYXiC4w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					<img src="<?= get_template_directory_uri() . '/img/img-video.jpg'; ?>" alt="">
					<span class="play-button"><img src="<?= get_template_directory_uri() . '/img/play-button.png'; ?>" alt="Play Button"></span>
				</div>
			</div>

			<div class="entre-em-contato">
				<div class="row">
					<div class="col-md-4 col-desktop">
						<div class="contato">
							<h2>entre em contato</h2>
							<p class="email texto">Email: gonzaga@esdrasobras.com.br</p>
							<p class="texto">Móvel: (41) 9638-9417</p>
							<p class="texto">Fixo: (41) 3538-9406</p>
						</div>
					</div>
					<div class="col-sm-8">
						<?= do_shortcode('[contact-form-7 id="22" title="Formulário de contato"]'); ?>
					</div>
					<div class="col-sm-4 col-mobile">
						<div class="contato">
							<h2>entre em contato</h2>
							<p class="email texto">Email: gonzaga@esdrasobras.com.br</p>
							<p class="texto">Móvel: (41) 9638-9417</p>
							<p class="texto">Fixo: (41) 3538-9406</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer();