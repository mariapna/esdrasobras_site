<?php

/**
 * Template Name: Sobre
 * Description: Página Sobre
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package esdrasobras
 */

get_header();

?>

<!-- PÁGINA SOBRE -->
<div class="pg pg-sobre">
	<div class="containerFull">
		<div class="sobre">

			<div class="texto-topo">
				<h1>conheça<br> nossa história</h1>
				<p class="texto">Somos sustentados por 3 pilares Honestidade, Pontualidade e Respeito ao
				Cliente que juntos chamados de Relacionamento Esdras.</p>
			</div>

			<figure class="img-sobre">
				<img src="<?= get_template_directory_uri() . '/img/img-pg-sobre.png'; ?>" alt="Imagem sobre">
			</figure>

			<figure class="img-desde">
				<img src="<?= get_template_directory_uri() . '/img/desde-1987.png'; ?>" alt="Imagem desde 1987">
			</figure>

			<div class="conheca">
				<h2 class="titulo-secundario">Conheça a esdras</h2>
				<div class="row">
					<div class="col-sm-6">
						<div class="texto-conheca left-text">
							<p class="texto">Fundada em 1987, a Esdras Obras é pioneira em manutenção de relacionamento, gerenciamento de mão de obra e construção civil.
								Com vasta experiência no setor civil, nasceu com a proposta de otimizar o tempo de execução de obras, oferecendo qualidade de vida aos funcionários e comodiadade para seus clientes empreiteiros.
							Inicio de atividade atendendo pequenas reformas e pavimentação de intertravados com o passar do tempo com a experiência adquirida e apoio dos clientes assumimos obras de maior porte mantendo um tripé de sustentação da empresa em HONESTIDADE, PONTUALIDADE e RESPEITO AO CLIENTE, com isto acreditamos manter um ótimo</p>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="texto-conheca right-text">
							<p class="texto">relacionamento com os clientes colocando-se sempre no seu lugar quanto a expectativa de conduta de um fornecedor. Esta experiência proporcionou observarmos a oportunidade de atender os construtores em um segmento complicado e de difícil atuação que toma muito tempo e preocupação em uma obra que é a mão de obra. Nosso maior e melhor produto é mão de obra profissional de qualidade para propiciar, tranquilidade e segurança quantos aos problemas rotineiros na gestão de pessoal, fazendo com que o cliente tenha total respaldo e soluções para concentrar-se somente na execução do seu projeto.</p>
						</div>
					</div>
				</div>
			</div>

			<figure class="img-sobre2">
				<img src="<?= get_template_directory_uri() . '/img/img-pg-sobre-2.png'; ?>" alt="Imagem sobre">
			</figure>

			<ul class="valores">
				<li class="valor">
					<h4 class="titulo-secundario">missão</h4>
					<p class="texto">Assegurar Excelência na gestão de pessoas no setor de engenharia cívil</p>
				</li>
				<li class="valor">
					<h4 class="titulo-secundario">visão</h4>
					<p class="texto">Ser estabelecida como melhor empresa especializada em gerenciamento de recursos humanos no setor de engenharia</p>
				</li>
				<li class="valor">
					<h4 class="titulo-secundario">valores</h4>
					<p class="texto">Relacionamento Transparente; Comprometimento com colaboradores associados; Agilidade e eficiência Simplicidade</p>
				</li>
			</ul>
		</div>
	</div>
</div>

<?php get_footer();