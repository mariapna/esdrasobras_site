<?php

/**
 * Template Name: Contato
 * Description: Página Contato
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package esdrasobras
 */

get_header();

?>

<!-- PÁGINA DE CONTATO -->
<div class="pg pg-contato">
	<div class="containerFull">
		<div class="contato">

			<div class="texto-topo">
				<h1>vamos conversar entre em contato.</h1>
			</div>

			<div class="mapa">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1800.8944008380347!2d-49.21272808495458!3d-25.478726771379566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dcfaaff1da3a77%3A0xd968a0935669a175!2sR.%20Marmelo%2C%20125%20-%20Uberaba%2C%20Curitiba%20-%20PR%2C%2081550-110!5e0!3m2!1spt-BR!2sbr!4v1573738858296!5m2!1spt-BR!2sbr" width="1200" height="408" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>

			<div class="entre-em-contato">
				<div class="row">
					<div class="col-sm-5 col-desktop">
						<div class="contato-info">
							<h5 class="titulo-secundario">RUA MARMELO, Nº125<br>UBERABA CURITIBA/PR</h5>

							<p class="texto">Horário de atendimento:<span>8:00 ÀS 17:00</span></p>

							<p class="texto">Esdras Obras, reformas & Pinturas LTDA CNPJ: 80386899/0001-61<br>Se preferir entre em contato pelo <span class="email">gonzaga@esdrasobras.com.br</span></p>

							<p class="titulo-secundario"><strong>(41) 4106-2114 - (41) 99911-9197</strong></p>

						</div>
					</div>
					<div class="col-sm-7">
						<?= do_shortcode('[contact-form-7 id="22" title="Formulário de contato"]'); ?>
					</div>
					<div class="col-sm-5 col-mobile">
						<div class="contato-info">
							<h5 class="titulo-secundario">RUA MARMELO, Nº125<br>UBERABA CURITIBA/PR</h5>

							<p class="texto">Horário de atendimento:<span>8:00 ÀS 17:00</span></p>

							<p class="texto">Esdras Obras, reformas & Pinturas LTDA CNPJ: 80386899/0001-61<br>Se preferir entre em contato pelo <span class="email">gonzaga@esdrasobras.com.br</span></p>

							<p class="titulo-secundario"><strong>(41) 4106-2114 - (41) 99911-9197</strong></p>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<?php get_footer();