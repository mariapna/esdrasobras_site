<?php

/**
 * Template Name: Portfólio
 * Description: Página Portfólio
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package esdrasobras
 */

get_header();

?>

<!-- PÁGINA PORTFOLIO -->
<div class="pg pg-portfolio">
	<div class="containerFull">
		<div class="portfolio">

			<div class="texto-topo">
				<h1>últimas obras</h1>
				<p class="texto">Conheça as ultimas Obras em que a Equipe Esdras esteve presente</p>
				<div class="button"><a href="#">entre em contato</a></div>
			</div>

			<ul class="projetos">
				<li class="projeto">
					<a href="#">
						<figure>
							<img src="<?= get_template_directory_uri() . '/img/projeto-1.png'; ?>" alt="Projeto">
							<figcaption class="hidden">Projeto</figcaption>
						</figure>
					</a>
					<h3 class="titulo-secundario">residencial milano</h3>
					<p class="texto">Apartamentos com 44m de área privativa, com vaga de garagem marcada. 2 Salões de festas com quadra poliesportiva e portaria 24 horas.</p>
					<a href="#" class="saiba-mais">saiba mais</a>
				</li>
				<li class="projeto">
					<a href="#">
						<figure>
							<img src="<?= get_template_directory_uri() . '/img/projeto-2.jpg'; ?>" alt="Projeto">
							<figcaption class="hidden">Projeto</figcaption>
						</figure>
					</a>
					<h3 class="titulo-secundario">residencial safira</h3>
					<p class="texto">Apartamentos com 44,50m2 de área privativa, em condomínio vertical fechado, com vaga de garagem. Quadra poliesportiva salão de festas e portaria 24 horas.</p>
					<a href="#" class="saiba-mais">saiba mais</a>
				</li>
				<li class="projeto">
					<a href="#">
						<figure>
							<img src="<?= get_template_directory_uri() . '/img/projeto-3.png'; ?>" alt="Projeto">
							<figcaption class="hidden">Projeto</figcaption>
						</figure>
					</a>
					<h3 class="titulo-secundario">construímos sua casa</h3>
					<p class="texto">Quer construir sua casa, mas não quer se preocupar com a execução do projeto, fala com a Esdras. Projetos particulares com prazo, garantia e qualidade.</p>
					<a href="#" class="saiba-mais">saiba mais</a>
				</li>
			</ul>

			<figure class="obras-embreve">
				<img src="<?= get_template_directory_uri() . '/img/mais-obras-em-breve.png'; ?>" alt="Mais obras em breve">
			</figure>
		</div>
	</div>
</div>

<?php get_footer();