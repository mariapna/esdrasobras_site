<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package esdrasobras
 */

?>

	<!-- RODAPÉ -->
	<footer>
		<div class="containerFull">
			<div class="rodape">
				<div class="row">

					<div class="col-sm-3">
						<figure class="logo-footer">
							<img src="<?= get_template_directory_uri() . '/img/logo.png' ?>" alt="Logo">
						</figure>
					</div>

					<div class="col-sm-2">
						<!-- <ul class="menu-footer">
							<li><a href="#">Home</a></li>
							<li><a href="#">Sobre</a></li>
							<li><a href="#">Serviços</a></li>
							<li><a href="#">Portfolio</a></li>
							<li><a href="#">Contato</a></li>
						</ul> -->
						<?php wp_nav_menu(); ?>
					</div>

					<div class="col-sm-2">
						<ul class="rede-social">
							<li>Facebook</li>
							<li>Twitter</li>
							<li>Instagram</li>
						</ul>
					</div>

					<div class="col-sm-5">
						<div class="newsletter">
							<h3>inscreva-se</h3>
							<?= do_shortcode('[wysija_form id="1"]'); ?>
						</div>
					</div>

				</div>
				<div class="copyright">
					<p>Powered by <a href="https://handgran.com/" target="_blank">Handgran</a> Ltd. © 2016 All Rights Reserved</p>
				</div>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>
</body>
</html>