$(function(){

	$(document).ready(function(){
		let videoSrc = $('.pg-servicos .servicos .video-como .video iframe').attr('src');
		videoSrc += '?&autoplay=1';
		$('.pg-servicos .servicos .video-como .video span.play-button').click(function(){
			$('.pg-servicos .servicos .video-como .video img').hide();
			$('.pg-servicos .servicos .video-como .video span.play-button').hide();
			$('.pg-servicos .servicos .video-como .video iframe').show();
			$('.pg-servicos .servicos .video-como .video iframe').attr('src', videoSrc);
		});
	});
		
});