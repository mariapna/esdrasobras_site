<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package esdrasobras
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- META -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content=""/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="" />

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?= get_template_directory_uri() . '/img/favicon.ico' ?>" /> 

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<!-- TOPO -->
	<header class="topo">
		<div class="containerFull">
			<div class="row">
				<!-- MENU MOBILE -->	
				<div class="col-sm-9 col-mobile">
					<div class="navbar" role="navigation">	
						<!--  MENU MOBILE-->
						<div class="row navbar-header">			
							<nav class="collapse navbar-collapse" id="collapse">
								<?php wp_nav_menu(); ?>
							</nav>						
						</div>	
						<!-- MENU MOBILE TRIGGER -->
						<button type="button" id="botao-menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
							<span class="sr-only">Menu</span>
						</button>		
					</div>
				</div>
				<!-- LOGO -->
				<div class="col-sm-3">
					<a href="<?= home_url(); ?>">
						<img class="img-responsive" src="<?= get_template_directory_uri() . '/img/logo.jpg'; ?>" alt="Logo Esdras Obras">
					</a>
				</div>
				<!-- MENU -->	
				<div class="col-sm-9 col-desktop">
					<div class="navbar" role="navigation">	
						<!-- MENU MOBILE TRIGGER -->
						<button type="button" id="botao-menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
							<span class="sr-only">Menu</span>
						</button>
						<!--  MENU MOBILE-->
						<div class="row navbar-header">			
							<nav class="collapse navbar-collapse" id="collapse">
								<?php wp_nav_menu(); ?>
							</nav>						
						</div>			
					</div>
				</div>
			</div>
		</div>
	</header>